package com.java8;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class TestLogFile {
	public static void main(String args[]) {

		try {
			// String fileName = "catalina.log";
			// String fileName = "catalina1.log";
			// String fileName = "catalina.log";
			 String fileName = "resource/testLog.log";
			 String startFrom = "Jul";

			try (BufferedReader br = Files.newBufferedReader(Paths.get(fileName))) {
				
				
				List<String> fileList = new ArrayList<>();
				
				List<String> list = new ArrayList<>();

				fileList = br.lines().collect(Collectors.toList());

				list = fileList.stream().filter(line -> line.startsWith(startFrom)).map(line -> {

					String[] str = line.split(",");

					String[] lineStartFromDateTime = str[1].split(" ");
					// System.out.println("No of Lines " + lineStartFromDateTime[0]);
					// System.out.println("No of str " +lineStartFromDateTime[1]);
					return lineStartFromDateTime[2];
				}).collect(Collectors.toList());

				DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm:ss");

				List<Integer> timeCount = new ArrayList<>();

				timeCount = list.stream().map(s -> LocalTime.parse(s, formatter).getHour())
						.collect(Collectors.toList());

				System.out.println("Total No of Lines   " + list.size());

				Map<Integer, Integer> result = timeCount.stream()
						.collect(Collectors.toMap(Function.identity(), i -> 1 * 1, (i1, i2) -> i1 + i2));
				result.forEach(
						(k, v) -> System.out.println(("Total hit between Hours " + k + "-" + (k + 1) + "-> " + v)));

			} catch (IOException e) {
				e.printStackTrace();
			}

			// fileList.forEach(System.out::println);

		} catch (Exception e) {
			System.out.println(e);
		}

	}

}
